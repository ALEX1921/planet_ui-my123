(async () => {
    const PAGE_SIZE = 10;

    function getPlanets(page = 1) {
        return fetch(`https://swapi.dev/api/planets?page=${page}`)
            .then(res => res.json())
            .then(({ count: total, results: entities }) => ({ total, entities }))
    }

    function getPlanet(url) {
        return fetch(url).then(res => res.json())
    }

    function getCardHtml(planet) {
        const items = ['diameter', 'population', 'gravity', 'terrain', 'climate']
            .map(key => `<li>${key}: ${planet[key]}</li>`)
            .join('');

        return `
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">${planet.name}</h5>
            <ul>
              ${items}
            </ul>
            <button data-link="${planet.url}" type="button" class="btn btn-primary js-open-modal" data-bs-toggle="modal" data-bs-target="#exampleModal">
              Подробнее
            </button>
          </div>
        </div>
      `;
    }

    function renderPage(planets) {
        const cards = planets
            .map(planet => getCardHtml(planet))
            .join('');

        document.querySelector('.js-cards').innerHTML = cards;
    }

    function renderPaginator(total, pageSize) {
        let items = '';
        for (let i = 0; i < total / pageSize; i++) {
            items += `<li class="page-item"><a class="page-link" href="#">${i + 1}</a></li>`
        }

        const listHtml = `
      <ul class="pagination">
        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
        ${items}
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
      </ul>
    `;

        document.querySelector('.js-paginator').innerHTML = listHtml;
    }

    function managePaginator() {
        document.querySelector('.js-paginator').addEventListener(
            'click',
            async event => {
                event.preventDefault();

                if (!event.target.classList.contains('page-link')) {
                    return;
                }

                const pageNumber = event.target.textContent;
                const { entities: planets } = await getPlanets(pageNumber);
                renderPage(planets);
            }
        );
    }

    async function getDataForModal(planetUrl) {
        /**
         * Получение информации от сервера
         */
        const planet = await getPlanet(planetUrl);
        const films = await Promise.all(
            planet.films.map(filmUrl => fetch(filmUrl).then(res => res.json()))
        );
        const people = await Promise.all(
            planet.residents.map(residentUrl => fetch(residentUrl).then(res => res.json()))
        );

        return { films, people };
    }

    function manageModal() {
        document.querySelector('.js-cards').addEventListener(
            'click',
            async event => {
                if (!event.target.classList.contains('js-open-modal')) {
                    return;
                }

                const url = event.target.getAttribute('data-link');
                const { films, people } = await getDataForModal(url);

                const filmsList = films.map(
                    film => ['episode_id', 'title', 'release_date'].map(
                        key => `<li>${key}: ${film[key]}</li>`
                    )
                );
                const peopleList = people.map(
                    person => ['name', 'gender', 'birth_year'].map(
                        key => `<li>${key}: ${person[key]}</li>`
                    )
                );
                let content = `
          <h2>Фильмы</h2>
          <ul>
            ${filmsList}
          </ul>
          <h2>Персонажи</h2>
          <ul>
            ${peopleList}
          </ul>
        `;

                document.querySelector('.js-modal-content').innerHTML = content;
            }
        );
    }

    const planetsInf = await getPlanets();
    renderPage(planetsInf.entities);
    renderPaginator(planetsInf.total, PAGE_SIZE);
    managePaginator();
    manageModal();
})();

